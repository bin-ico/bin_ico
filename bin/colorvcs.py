#!/usr/bin/env python
import sys
import re


class Colors:
    (
        black,
        red,
        green,
        brown,
        blue,
        magenta,
        cyan,
        white,
    ) = range(8)
    bright = 100
    yellow = brown + bright
    gray = black + bright  # ! highlighting on black bground is invisible


class Categories:
    (
        update,
        modified,
        conflict,
        merge,
        add,
        delete,
        unknown,
        ignored,
        error,
        replace,
        external,
        other,
        diff_add,
        diff_del,
        diff_file,
        diff_chunk,
    ) = range(1, 17)
    rename = replace


# category -> color
theme = {
    Categories.update: Colors.green,
    Categories.modified: Colors.red,
    Categories.conflict: Colors.yellow,
    Categories.merge: Colors.blue + Colors.bright,
    Categories.add: Colors.cyan,
    Categories.delete: Colors.magenta,
    Categories.unknown: Colors.gray,
    Categories.ignored: Colors.blue,
    Categories.error: Colors.yellow,
    Categories.other: Colors.white,
    Categories.external: Colors.brown,
    Categories.replace: Colors.green + Colors.bright,
    Categories.diff_add: Colors.green,
    Categories.diff_del: Colors.red,
    Categories.diff_file: Colors.white+Colors.bright,
    Categories.diff_chunk: Colors.yellow,
}


class Colorer:
    all = {}

    def __init__(self, name, regexp, table, whole=False):
        self.all[name] = self
        self.regexp = re.compile(regexp)
        self.table = table
        self.whole = whole
        self.name = name

    def alias(self, name):
        self.all[name] = self

    def check(self, line):
        status = None
        m = self.regexp.match(line)
        if m:
            # print `m.group(1)`, self.name
            g = m.group(1)
            if self.whole:
                status = self.table.get(g)
            else:
                for c in g:
                    status = self.table.get(c)
                    if status:
                        break
        return status


class Chain(Colorer):
    def __init__(self, name, *colorers):
        self.all[name] = self
        self.colorers = colorers

    def check(self, line):
        status = None
        for c in self.colorers:
            status = c.check(line)
            if status:
                break
        return status


Colorer('svn', r'^([ A-Z?!~]{2}) +\S+', {
    'U': Categories.update,
    'C': Categories.conflict,
    'G': Categories.merge,
    'M': Categories.modified,
    'A': Categories.add,
    'R': Categories.replace,
    'D': Categories.delete,
    'I': Categories.ignored,
    'X': Categories.external,
    '?': Categories.unknown,
    '!': Categories.error,
    '~': Categories.error,
})

Colorer('cvs', r'^(\S) \S+', {
    'P': Categories.update,
    'U': Categories.update,
    'C': Categories.conflict,
    'N': Categories.add,
    'L': Categories.ignored,
    'M': Categories.modified,
    'A': Categories.add,
    'R': Categories.delete,
    'I': Categories.ignored,
    '?': Categories.unknown,
    'warning': Colors.bright+Colors.cyan
})


Colorer('diffC', r'^(diff|Only|[<>])', {
    # 'Index': Categories.diff_file,
    'diff': Categories.diff_file,
    'Only': Categories.diff_file,
    '>': Categories.diff_add,
    '<': Categories.diff_del,
}, whole=True).alias('diff')

Colorer('diffU', r'^([-+@=]+)', {
    '===': Categories.diff_file,
    '+++': Categories.diff_add,
    '---': Categories.diff_del,
    '=': Categories.diff_file,
    '+': Categories.diff_add,
    '-': Categories.diff_del,
    '@@': Categories.diff_chunk,
    '@': Categories.diff_chunk,
}, whole=True)

bzru = Colorer('bzrupd', r'^([- A-Z?+*]{1,3}) +\S+', {
    # Categories.update
    # Column 1 - versioning/renames:
    '+': Categories.add,  # File versioned
    '-': Categories.other,  # File unversioned
    'R': Categories.rename,  # File renamed
    '?': Categories.unknown,  # File unknown
    'C': Categories.conflict,  # File has conflicts
    'P': Categories.other,  # Entry for a pending merge (not a file)
    # Column 2 - contents:
    'N': Categories.add,  # File created
    'D': Categories.delete,  # File deleted
    'K': Categories.other,  # File kind changed
    'M': Categories.modified,  # File modified
    # Column 3 - execute:
    '*': Categories.other,  # The execute bit was changed
    # Categories.ignored ?
})

bzrs = Colorer('bzrstat', r'^(\w+):', {
    'added': Categories.add,
    'modified': Categories.modified,
    'renamed':  Categories.rename,
    'removed':  Categories.delete,
    'unknown':  Categories.unknown,
    'conflicts': Categories.conflict,
}, whole=True)

Chain('bzr', bzru, bzrs)


hgs = Colorer('hgstat', r'^(\S) \S+', {
    'A': Categories.add,
    'M': Categories.modified,
    'R': Categories.delete,
    '?': Categories.unknown,
    'I': Categories.ignored,
})

hgresolv = Colorer('hgresolv', r'^(\S) \S+', {
    'U': Categories.conflict
})

Chain('hg', hgs, hgresolv)

gitstat = Colorer('git', r'^([ A-Z?!]{1,2}) +\S+', {
    # see git help status
    # Column 1 - there, Column2: here
    '?': Categories.unknown,  # File unknown
    'M': Categories.modified,  # File modified
    'A': Categories.add,  # File created
    'D': Categories.delete,  # File deleted
    'R': Categories.rename,  # File renamed
    # Categories.ignored ?
})


ESC = chr(27)


def clr(fg):
    hi = 0
    if fg is not None:
        hi = fg >= Colors.bright
        if hi:
            fg -= Colors.bright
    return _clr(fg, hi=hi)


def _clr(fg, bg=0, hi=0):
    'see colortable8x16.py'
    if fg is None:
        fg = 0
    else:
        fg += 30
        bg += 40
    return ESC+'[%(hi)d;%(fg)d;%(bg)dm' % locals()


colorer = None
if sys.argv[1:]:
    colorer = Colorer.all.get(sys.argv[1])

if not colorer:
    ks = Colorer.all.keys()
    ks.sort()
    print(
        'usage: colorvcs.py type where type can be:',
        ' '.join(ks),
        'filters stdin to stdout'
    )
    raise SystemExit(1)

for line in sys.stdin:
    line = line.rstrip()
    status = colorer and line.strip() and colorer.check(line)
    if status:
        print(clr(theme.get(status)) + line + clr(None))
    else:
        print(line)
