set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

let g:python3_host_prog = "/usr/bin/python3"


" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }

" Plug 'vim-python/python-syntax'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

set guicursor=i:block
