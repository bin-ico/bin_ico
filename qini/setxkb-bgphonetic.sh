#!/bin/bash

#same as scrollock sclk_toggle above
xmodmap -e "keysym Pause = ISO_Next_Group"
#xmodmap -e "keysym Break = ISO_Next_Group"
xmodmap ~/.Xmodmap

xset r rate 200 30

xmodmap -pk | grep -q Alt_R || xmodmap -e 'keycode 108 = Alt_R'
#xmodmap -e "add mod1 = Alt_L Alt_R"
