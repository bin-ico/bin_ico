#!/usr/bin/python3
import sys


# a TERM entry for each termtype that is colorizable
TERMINAL_TYPES = (
    'linux',
    'linux-c',
    'mach-color',
    'console',
    'con132x25',
    'con132x30',
    'con132x43',
    'con132x60',
    'con80x25',
    'con80x28',
    'con80x30',
    'con80x43',
    'con80x50',
    'con80x60',
    'cygwin',
    'dtterm',
    'putty',
    'xterm',
    'xterm-color',
    'xterm-debian',
    'rxvt',
    'screen',
    'screen-bce',
    'screen-w',
    'vt100',
    'Eterm',
    'konsole',
    'konsole-16',
)


# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=NONE 01=BOLD 04=UNDERSCORE 05=BLINK 07=REVERSE 08=CONCEALED
# Text color codes:
# 30=BLACK 31=RED 32=GREEN 33=YELLOW 34=BLUE 35=MAGENTA 36=CYAN 37=WHITE
# Background color codes:
# 40=BLACK 41=RED 42=GREEN 43=YELLOW 44=BLUE 45=MAGENTA 46=CYAN 47=WHITE

# Color attributes
NONE = 0
BOLD = 1
UNDERSCORE = 4
BLINK = 5
REVERSE = 7
CONCEALED = 8

# Text colors
BLACK = 30
RED = 31
GREEN = 32
YELLOW = 33
BLUE = 34
MAGENTA = 35
CYAN = 36
WHITE = 37

# Background colors
BG_BLACK = 40
BG_RED = 41
BG_GREEN = 42
BG_YELLOW = 43
BG_BLUE = 44
BG_MAGENTA = 45
BG_CYAN = 46
BG_WHITE = 47


class Type:
    NORMAL = 'no', NONE                 # 00 # global default
    FILE = 'fi', NONE                   # 00 # normal file
    DIR = 'di', BLUE, BOLD              # 01;34 # directory
    LINK = 'ln', CYAN, BOLD             # 01;36 # symbolic link. (If you set this to 'target' instead of a
                                        # numerical value, the color is as for the file pointed to.)
    FIFO = 'pi', YELLOW                 # 40;33 # pipe
    SOCK = 'so', MAGENTA, BOLD          # 01;35 # socket
    DOOR = 'do', SOCK                   # 01;35 # door
    BLK = 'bd', YELLOW, BOLD            # 40;33;01 # block device driver
    CHR = 'cd', YELLOW, BOLD            # 40;33;01 # character device driver
    ORPHAN = 'or', RED, BOLD            # 40;31;01 # symlink to nonexistent file
    SETUID = 'su', WHITE, BG_RED        # 37;41 # file that is setuid (u+s)
    SETGID = 'sg', BLACK, BG_YELLOW     # 30;43 # file that is setgid (g+s)
    STICKY_OTHER_WRITABLE = 'tw', DIR   # 30;42 # dir that is sticky and other-writable (+t,o+w)
    OTHER_WRITABLE = 'ow', DIR          # 34;42 # dir that is other-writable (o+w) and not sticky
    STICKY = 'st', DIR, BG_GREEN        # 37;44 # dir with the sticky bit set (+t) and not other-writable
    EXEC = 'ex', GREEN                  # 01;32 # files with execute permission


class Suffix:
    DOS_EXEC = dict(
        extensions=('exe', 'com', 'bat', 'btm', 'cmd'),
        colors=Type.EXEC,
    )
    ARCHIVE = dict(
        extensions=(
            'zip', 'tar', 'tar.gz', 'tgz', 'tar.bz2', 'tbz2', 'tbz', 'gz', 'bz2', 'rar',
            '7z', 'lzma', 'xz', 'txz', 'tlz', 'lz', 'Z', 'deb', 'rpm', 'jar', 'war',
            'ear', 'sar', 'rar', 'ace', 'zoo', 'cpio', '7z', 's7z',
        ),
        colors=RED,
    )
    IMAGE = dict(
        extensions=(
            'jpg', 'jpeg', 'gif', 'bmp', 'pbm', 'pgm', 'ppm', 'tga', 'xbm', 'xpm', 'tif',
            'tiff', 'png', 'svg', 'svgz', 'mng', 'pcx', 'mov', 'mpg', 'mpeg', 'm2v', 'mkv',
            'ogm', 'mp4', 'm4v', 'mp4v', 'vob', 'qt', 'nuv', 'wmv', 'asf', 'rm', 'rmvb',
            'flc', 'avi', 'fli', 'flv', 'gl', 'dl', 'xcf', 'xwd', 'yuv', 'cgm', 'emf',
            'axv', 'anx', 'ogv', 'ogx',
        ),
        colors=MAGENTA,
    )
    SOUND = dict(
        extensions=(
            'aac', 'au', 'flac', 'mid', 'midi', 'mka', 'mp3', 'mpc', 'ogg', 'ra', 'wav',
            'axa', 'oga', 'spx', 'xspf',
        ),
        colors=MAGENTA,
    )
    BINARY_OBJ = dict(
        extensions=('pyc', 'obj', 'o', 'so', 'a', 'dll'),
        colors=(BLACK, BOLD),
    )


class FMT4dircolors:
    keys = ' '
    values = ';'
    items = '\n'
    suffix_pfx = '.'
    use_abbr = False


class FMT4ls:
    keys = '='
    values = ';'
    items = ':'
    suffix_pfx = '*.'
    use_abbr = True


FMT = FMT4dircolors


def type_valueonly_flat(values, ignore_abbr=1): # recursive lookup/flatten
    r = []
    for v in values[ignore_abbr:]:
        if isinstance(v, tuple):
            r += type_valueonly_flat(v)
        else:
            r.append(v)
    return r


def types():
    res = []
    for k, value in Type.__dict__.items():
        if k.startswith('_'):
            continue
        abbr = value[0]
        name = abbr if FMT.use_abbr else k
        res.append([name, type_valueonly_flat(value)])
    return res


def itemize(pairs):
    items = []
    for k, c in pairs:
        if k.startswith('_') or not c:
            continue
        if isinstance(c, (tuple, list)):
            values = [str(a) for a in c]
        else:
            values = [str(c)]
        values = FMT.values.join(values)
        items.append(FMT.keys.join((k, values)))
    return items


def suffixes(extensions, colors):
    extensions = list(extensions)
    extensions += [x.lower() for x in extensions]
    extensions += [x.upper() for x in extensions]
    extensions = sorted(set(extensions))
    if not isinstance(colors, (tuple, list)):
        colors = [colors]
    c = type_valueonly_flat(colors, ignore_abbr=0)
    return ((FMT.suffix_pfx + key, c) for key in extensions)


do_dir = do_ls = False
do_dosexe = False
for a in sys.argv[1:]:
    if a.startswith('-dircolor'):
        do_dir = True
    if a.startswith('-ls'):
        do_ls = True
    if a.startswith('-dosexe'):
        do_dosexe = True

if do_ls == do_dir:
    raise SystemExit(
        help + ', '.join(
            k for k in Suffix.__dict__.iterkeys()
            if not k.startswith('_')
        )
    )

if not do_dosexe:
    del Suffix.DOS_EXEC

p = []
if do_dir:
    FMT = FMT4dircolors
    p += itemize(('TERM', a) for a in TERMINAL_TYPES)
else:   # do_ls
    # ignore term-check -- don't be too smart
    FMT = FMT4ls

p += itemize(types())
for k, s in Suffix.__dict__.items():
    if not k.startswith('_'):
        p += itemize(suffixes(s['extensions'], s['colors']))

print(FMT.items.join(p))
